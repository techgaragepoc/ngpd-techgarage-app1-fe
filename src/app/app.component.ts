import { Component } from '@angular/core';
import { environment } from '../environments/environment';
// import { Http, Headers, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private _http: HttpClient) { }
  title = 'app works!';
  token = '';
  algo = 'hs256';
  verified = false;

  // Internal functions
  initialData = () => {
    this.token = '';
    this.verified = false;
    this.algo = 'hs256';
  }
  // Events
  loginViaHSAlgo = (control) => {
    this.initialData();

    let headers = new HttpHeaders({ 'Access-Control-Allow-Origin': window.location.origin });
    // headers = headers.append('apikey', environment.jwtApiKey);
    this.algo = 'hs256';

    const httpOptions = {
      headers: headers,
      // withCredentials: true
      // observe: 'response'
    };

    // console.log('http headers', headers);

    // this._http.post<any>(environment.jwtHS256EndPoint, { username: 'pankaj-sabharwal' }, httpOptions)
    this._http.post<any>(environment.jwtHS256EndPoint, { username: 'pankaj-sabharwal' })
      .subscribe(
      (response) => {
        // console.log('response', response);
        // control.value = response.token;
        this.token = response.jwt;
        // this._ChangeDetectorRef.markForCheck()
      },
      (error) => {
        console.log('error', error);
      });
  }

  loginViaRSAlgo = (control) => {
    this.initialData();

    this.algo = 'rs256';

    // this._http.post<HttpResponse<Object>>(environment.jwtRS256EndPoint, { observe: 'response' })
    this._http.post<any>(environment.jwtRS256EndPoint, { username: 'pankaj-sabharwal' })
      .subscribe(
      (response) => {
        // console.log('response', response); // .get('jwt-token'));
        // control.value = response.token;
        this.token = response.token;
      },
      (error) => {
        console.log('error', error);
      });
  }

  verifyRS256Token = (token) => {
    let headers = new HttpHeaders({ jwt: token });

    const httpOptions = {
      headers: headers,
      // withCredentials: true 
      // observe: 'response'
    };

    this._http.post<any>(environment.jwtVerifyRS256EndPoint, { username: 'pankaj-sabharwal' }, httpOptions)
      .subscribe(
      (response) => {
        // console.log('response', response); // .get('jwt-token'));
        // control.value = response.token;
        console.log('response', response);
        this.verified = (response.headers && response.headers.Jwt === token);
      },
      (error) => {
        console.log('error', error);
      });

  }
}
