import { Component, Input, Output, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-auth-view-component',
  templateUrl: './auth-view.component.html',
  styleUrls: ['./auth-view.component.css'],
  // encapsulation: ViewEncapsulation.Emulated
})
export class AuthViewComponent implements OnInit {
  _authToken: String = '';
  _algo: String = 'hs256';

  get environment() {
    return environment;
  }

  constructor(private _changeDetectorRef: ChangeDetectorRef) { }

  @Input()
  set Token(value) {
    this._authToken = value;
    this._changeDetectorRef.detectChanges();
  }
  get Token() {
    return this._authToken;
  }

  @Input()
  set Algorithm(value) {
    this._algo = value;
    this._changeDetectorRef.detectChanges();
  }
  get Algorithm() {
    return this._algo;
  }

  ngOnInit() {
  }
}
